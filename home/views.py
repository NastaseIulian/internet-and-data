
# from home.models import HomePage
from django.shortcuts import render
from django.views.generic import TemplateView


# class HomeTemplateView(TemplateView):
#     template_name = 'home/home.html'
from home.models import Carousel, UnderDetail, MidleDetail


def home_page(request):
    carousel = Carousel.objects.all()
    under = UnderDetail.objects.all()
    midle = MidleDetail.objects.all()
    context = {
        'carousel': carousel,
        'under': under,
        'midle': midle

    }
    return render(request,'home/home.html',context)
