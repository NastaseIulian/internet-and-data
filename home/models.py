from django.db import models

class Carousel(models.Model):
    short_description = models.CharField(max_length=100)
    image = models.ImageField(upload_to='static/home')


    def __str__(self):
        return f'{self.short_description}'

class UnderDetail(models.Model):
    tilte = models.CharField(max_length=100)
    image = models.ImageField(upload_to='static/home')
    description = models.TextField()
    link = models.CharField(max_length=100,null=True)

    def __str__(self):
        return f'{self.tilte}'


class MidleDetail(models.Model):
    image = models.ImageField(upload_to='static/home')
    step = models.CharField(max_length=10)
    make = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.step}'