from django.contrib import admin

from home.models import Carousel, UnderDetail, MidleDetail

admin.site.register(Carousel)
admin.site.register(UnderDetail)
admin.site.register(MidleDetail)
