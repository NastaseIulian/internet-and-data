
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include ('home.urls')),
    path('', include('internet_and_data.urls')),
    
]
