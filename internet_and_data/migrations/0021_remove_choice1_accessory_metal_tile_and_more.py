# Generated by Django 4.0.4 on 2022-06-21 11:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('internet_and_data', '0020_choice1_choice2_choice3_choice4_choice5_choice6_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='choice1',
            name='accessory_metal_tile',
        ),
        migrations.RemoveField(
            model_name='choice1',
            name='accessory_tile',
        ),
        migrations.RemoveField(
            model_name='choice2',
            name='accessory_metal_tile',
        ),
        migrations.RemoveField(
            model_name='choice2',
            name='accessory_tile',
        ),
        migrations.RemoveField(
            model_name='choice3',
            name='accessory_metal_tile',
        ),
        migrations.RemoveField(
            model_name='choice3',
            name='accessory_tile',
        ),
        migrations.RemoveField(
            model_name='choice4',
            name='accessory_metal_tile',
        ),
        migrations.RemoveField(
            model_name='choice4',
            name='accessory_tile',
        ),
        migrations.RemoveField(
            model_name='choice5',
            name='accessory_metal_tile',
        ),
        migrations.RemoveField(
            model_name='choice5',
            name='accessory_tile',
        ),
        migrations.RemoveField(
            model_name='choice6',
            name='accessory_metal_tile',
        ),
        migrations.RemoveField(
            model_name='choice6',
            name='accessory_tile',
        ),
        migrations.AddField(
            model_name='choice1',
            name='accessory',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='choice2',
            name='accessory',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='choice3',
            name='accessory',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='choice4',
            name='accessory',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='choice5',
            name='accessory',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='choice6',
            name='accessory',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
