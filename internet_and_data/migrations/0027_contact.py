# Generated by Django 4.0.4 on 2022-06-23 13:13

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('internet_and_data', '0026_alter_photovoltaic_consumption_file_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Nume')),
                ('email', models.EmailField(max_length=254, verbose_name='E-mail')),
                ('phone', models.CharField(max_length=20, verbose_name='Telefon')),
                ('message', models.TextField(max_length=1000, verbose_name='Mesaj')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Creat la data')),
            ],
        ),
    ]
