# Generated by Django 4.0.4 on 2022-06-21 08:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('internet_and_data', '0019_photovoltaicfirstdetails'),
    ]

    operations = [
        migrations.CreateModel(
            name='Choice1',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='static/choices')),
                ('number_of_panels', models.IntegerField()),
                ('accessory_tile', models.CharField(max_length=200)),
                ('accessory_metal_tile', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
                ('price_with_consumption_file', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
        ),
        migrations.CreateModel(
            name='Choice2',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='static/choices')),
                ('number_of_panels', models.IntegerField()),
                ('accessory_tile', models.CharField(max_length=200)),
                ('accessory_metal_tile', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
                ('price_with_consumption_file', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
        ),
        migrations.CreateModel(
            name='Choice3',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='static/choices')),
                ('number_of_panels', models.IntegerField()),
                ('accessory_tile', models.CharField(max_length=200)),
                ('accessory_metal_tile', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
                ('price_with_consumption_file', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
        ),
        migrations.CreateModel(
            name='Choice4',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='static/choices')),
                ('number_of_panels', models.IntegerField()),
                ('accessory_tile', models.CharField(max_length=200)),
                ('accessory_metal_tile', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
                ('price_with_consumption_file', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
        ),
        migrations.CreateModel(
            name='Choice5',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='static/choices')),
                ('number_of_panels', models.IntegerField()),
                ('accessory_tile', models.CharField(max_length=200)),
                ('accessory_metal_tile', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
                ('price_with_consumption_file', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
        ),
        migrations.CreateModel(
            name='Choice6',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(upload_to='static/choices')),
                ('number_of_panels', models.IntegerField()),
                ('accessory_tile', models.CharField(max_length=200)),
                ('accessory_metal_tile', models.CharField(max_length=200)),
                ('price', models.DecimalField(decimal_places=2, max_digits=15)),
                ('price_with_consumption_file', models.DecimalField(decimal_places=2, max_digits=15)),
            ],
        ),
        migrations.AlterField(
            model_name='photovoltaic',
            name='roof_type',
            field=models.CharField(choices=[('tile', 'Țiglă'), ('metal_tile', 'Țiglă Metalică')], max_length=30),
        ),
    ]
