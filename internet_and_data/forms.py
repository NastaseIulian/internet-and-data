from django import forms
from django.forms import Select, TextInput, EmailInput, Textarea

from internet_and_data.models import Photovoltaic, Contact


class PhotovoltaicForm(forms.ModelForm):
    class Meta:
        model = Photovoltaic
        fields = '__all__'
        widgets = {
            'monthly_consum': Select(attrs={'class':'form-control'}),
            'roof_type': Select(attrs={'class':'form-control'})
        }

class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name','email','phone','message']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Numele tău', 'class':'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Adresa ta de email ', 'class':'form-control'}),
            'phone': TextInput(attrs={'placeholder': 'Numărul tău de telefon', 'class':'form-control'}),
            'message': Textarea(attrs={'placeholder': 'Mesajul tău', 'class':'form-control','rows':4})
        }
