from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.html import strip_tags

from finalProject import settings


class InternetData(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(upload_to='static/internet_data')

    def __str__(self):
        return f'{self.name}'


class Photovoltaic(models.Model):
    MONTHLY_CONSUM_CHOICES = (('up_to_500 kW_monthly', 'Până la 500 kW/lună'),
                              ('between_500_and_1000 kW_monthly', ' Între 500 și 1000 kW/lună'),
                              ('over_1000 kW_monthly', 'Peste 1000 kW/lună'))
    ROOF_TYPE_CHOICES = (('tile', 'Țiglă'), ('metal_tile', 'Țiglă Metalică'))

    monthly_consum = models.CharField('Consum lunar', max_length=50, choices=MONTHLY_CONSUM_CHOICES)
    roof_type = models.CharField('Tip acoperiș', max_length=30, choices=ROOF_TYPE_CHOICES)
    consumption_file = models.BooleanField('Vreau Dosar Prosumator', default=False)

    def __str__(self):
        return f'{self.monthly_consum}'


class PhotovoltaicProduct(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True)
    technical_details = models.TextField(null=True)
    production_details = models.TextField(null=True)
    price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    image = models.ImageField(upload_to='static/photovoltaics')

    def __str__(self):
        return f'{self.name}'


class PhotovoltaicFirstDetails(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='static/photovoltaics')
    description = models.TextField(null=True)

    def __str__(self):
        return f'{self.name}'


class PhotovoltaicKit(models.Model):
    ACCESSORY_TYPE_CHOICES = (('tile', 'Șine, cleme și prinderi: speciale pentru țiglă'),
                              ('metal_tile', 'Șine, cleme și prinderi: speciale pentru țiglă metalică'))

    power = models.CharField(max_length=100, choices=Photovoltaic.MONTHLY_CONSUM_CHOICES)
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='static/choices')
    number_of_panels = models.IntegerField()
    accessory = models.CharField(max_length=100, choices=ACCESSORY_TYPE_CHOICES)
    price = models.DecimalField(max_digits=15, decimal_places=2)
    price_with_consumption_file = models.DecimalField(max_digits=15, decimal_places=2)

    def __str__(self):
        return f'{self.power}'


class GeneralSupport(models.Model):
    image = models.ImageField(upload_to='static/support')
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return f'{self.name}'


class DetailSupport(models.Model):
    image = models.ImageField(upload_to='static/support')
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return f'{self.name}'


class Contact(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nume')
    email = models.EmailField(verbose_name='E-mail')
    phone = models.CharField(max_length=20, verbose_name='Telefon')
    message = models.TextField(max_length=1000, verbose_name='Mesaj')
    date_created = models.DateTimeField(verbose_name='Creat la data', default=timezone.now)

    def __str__(self):
        return f'{self.email} - {self.date_created.date()}'



@receiver(post_save, sender=Contact)
def send_email_for_contact_created(sender, instance: Contact, created, **kwargs):
    if created:
        subject = 'Mesaj client Next Level'
        msg = f'''Salut,
       <br>
       <br> Mesaj nou primit in: <b> {instance.date_created.date()} </b>, ora <b> {instance.date_created.time()} </b>.
       <br> Nume: <b> {instance.name} </b>
       <br> Email: <b> {instance.email} </b>
       <br> Telefon: <b> {instance.phone} </b>
       <br><br>
        Mesaj:
        <br>
        {instance.message}.
        '''
        plain_msg = strip_tags(msg)
        send_mail(
            subject=subject,
            message=plain_msg,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=[settings.RECIPIENT_ADDRESS],
            html_message=msg
        )
