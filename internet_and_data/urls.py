
from django.urls import path

from internet_and_data import views

urlpatterns = [
    path('list-of-products/', views.InternetDataListView.as_view(), name='list_of_products'),
    path('create-request/', views.PhotovoltaicCreateView.as_view(), name='create_form'),
    path('photovoltaic-products/', views.first_detail_view, name='photovoltaic_products'),
    # path('photovoltaic-products/', views.PhotovoltaicListView.as_view(), name='photovoltaic_products'),
    path('kit-configuration/', views.choices_view, name='all_choices'),
    path('support/', views.support_view, name='support'),
    path('contact/', views.ContactCreateView.as_view(), name='contact')


]