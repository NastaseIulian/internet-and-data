from django.contrib import admin
#
from internet_and_data.models import InternetData, PhotovoltaicProduct, PhotovoltaicFirstDetails, PhotovoltaicKit, \
    GeneralSupport, DetailSupport, Photovoltaic, Contact
# Choice2, \
    # Choice3, Choice4, Choice5, Choice6
#
# admin.site.register(InternetData)

from django import forms
from django.contrib import admin
from ckeditor.widgets import CKEditorWidget


class InternetDataAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = InternetData
        fields = '__all__'


# class PhotovoltaicProductAdminForm(forms.ModelForm):
#     description = forms.CharField(widget=CKEditorWidget())
#     technical_details = forms.CharField(widget=CKEditorWidget())
#     production_details = forms.CharField(widget=CKEditorWidget())

    # class Meta:
    #     model = PhotovoltaicProduct
    #     fields = '__all__'


class InternetDataAdmin(admin.ModelAdmin):
    form = InternetDataAdminForm


# class PhotovoltaicProductAdmin(admin.ModelAdmin):
#     form = PhotovoltaicProductAdminForm


admin.site.register(InternetData, InternetDataAdmin)
admin.site.register(Photovoltaic)
admin.site.register(PhotovoltaicProduct)
admin.site.register(PhotovoltaicFirstDetails)
admin.site.register(PhotovoltaicKit)
admin.site.register(GeneralSupport)
admin.site.register(DetailSupport)
admin.site.register(Contact)






# admin.site.register(Choice2)
# admin.site.register(Choice3)
# admin.site.register(Choice4)
# admin.site.register(Choice5)
# admin.site.register(Choice6)
