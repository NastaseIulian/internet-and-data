from django.apps import AppConfig


class InternetAndDataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'internet_and_data'
