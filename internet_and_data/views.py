from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView

from internet_and_data.forms import PhotovoltaicForm, ContactForm
from internet_and_data.models import InternetData, Photovoltaic, PhotovoltaicProduct, PhotovoltaicFirstDetails, \
    PhotovoltaicKit, GeneralSupport, DetailSupport, Contact

class InternetDataListView(ListView):
    template_name = 'internet_and_data/list_of_products.html'
    model = InternetData
    queryset = InternetData.objects.all()
    context_object_name = 'all_products'


class PhotovoltaicCreateView(CreateView):
    template_name = 'internet_and_data/create_form.html'
    model = Photovoltaic
    form_class = PhotovoltaicForm
    success_url = reverse_lazy('all_choices')

    def form_valid(self, form):
        valid = super().form_valid(form)
        print(form.cleaned_data)
        power = form.cleaned_data['monthly_consum']
        accessory = form.cleaned_data['roof_type']
        product_kit = PhotovoltaicKit.objects.filter(power=power, accessory=accessory)

        context = {
            'product_kit': product_kit,
            'consumption_file': form.cleaned_data.get('consumption_file', False)
        }
        return render(self.request, 'internet_and_data/all_choices.html', context)


class ContactCreateView(CreateView):
    model = Contact
    template_name = 'internet_and_data/contact_form.html'
    form_class = ContactForm

    def get_success_url(self):
        url = reverse_lazy('contact')
        print(url)
        if self.request.method == 'POST':
            print('post')
            return url + '?success=1'
        else:
            print('get')
            return url

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['success'] = self.request.GET.get('success', 0)
        return data





# class PhotovoltaicListView(ListView):
#     template_name = 'internet_and_data/photovoltaic_products.html'
#     model = PhotovoltaicProduct
#     queryset = PhotovoltaicProduct.objects.all()
#     context_object_name = 'voltaic_products'
#
# class PhotovoltaicFirstDetailsListView(ListView):
#     template_name = 'internet_and_data/photovoltaic_products.html'
#     model = PhotovoltaicFirstDetails
#     queryset = PhotovoltaicFirstDetails.objects.all()
#     context_object_name = 'voltaic_first_details'

# inlocuiesc cele 2 clase de mai sus (listView's) cu functia de mai jos, pentru a putea
# afisa 2 modele intr-o singura pagina html.

def first_detail_view(request):
    detail = PhotovoltaicFirstDetails.objects.all()
    products = PhotovoltaicProduct.objects.all()
    context = {
        'detail': detail,
        'products': products
    }
    return render(request, 'internet_and_data/photovoltaic_products.html', context)


def choices_view(request):
    product_kit = PhotovoltaicKit.objects.all()
    context = {
        'product_kit': product_kit,
    }
    return render(request, 'internet_and_data/all_choices.html', context)


def support_view(request):
    general = GeneralSupport.objects.all()
    detail = DetailSupport.objects.all()
    context = {
        'general': general,
        'detail': detail
    }
    return render(request, 'internet_and_data/support.html', context)
